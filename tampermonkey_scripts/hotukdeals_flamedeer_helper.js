// ==UserScript==
// @name         Hotukdeals Flamedeer Helper
// @namespace    sim99.co.uk
// @version      0.1
// @downloadURL  https://bitbucket.org/sim099/public_stuff/raw/master/tampermonkey_scripts/hotukdeals_flamedeer_helper.js
// @description  Aids with catching of flamedeer by adding an information box on screen, showing if a flamedeer is available on the current page and a countdown to it's disappearance.
// @author       sim099
// @match        *://hotukdeals.com/*
// @match        *://www.hotukdeals.com/*
// @grant        none
// @run-at       document-body
// ==/UserScript==
(function() {
    'use strict';

    console.log("Hotukdeals Flamedeer Helper loaded...");
})();
