// ==UserScript==
// @name         Timer Tracker
// @namespace    sim99.co.uk
// @version      1.0
// @downloadURL  https://bitbucket.org/sim099/public_stuff/raw/master/tampermonkey_scripts/timer_tracker.js
// @description  Keeps track of all timers created by a SetTimer or setTimeout JavaScript call. Maintains a collection of objects within window.timers, each comtaining type, id, delay, params, func and cleared. The func attribute is a string representation of the function to be executed and cleared indicates whether the timer was cleared by a subsequent clear(Timeout|Interval) call.
// @author       sim099
// @match        *://*
// @grant        none
// @run-at       document-start
// ==/UserScript==
(function() {
    'use strict';

    var originalSetTimeout = window.setTimeout;
    var originalClearTimeout = window.clearTimeout;

    var originalSetInterval = window.setInterval;
    var originalClearInterval = window.clearInterval;

    if (!window.timers) { window.timers = {}; }

    function __addTimer(type, id, func, delay, ...params) {
        var timerId = type + '_' + id;
        window.timers[timerId] = {
            'type': type,
            'id': id,
            'delay': delay,
            'params': params,
            'func': new String(func).valueOf(),
            'cleared': false
        };
        __logTimer(type, id, func, delay, ...params);
    }

    function __clearTimer(type, id) {
        var timerId = type + '_' + id;
        if (id != undefined) {
            window.timers[timerId].cleared = true;
        }
    }

    window.setTimeout = function(func, delay, ...params) {
        var id = originalSetTimeout(func, delay, ...params);
        __addTimer('timeout', id, func, delay, ...params);
        return id;
    };

    window.clearTimeout = function(id) {
        __clearTimer('timeout', id);
        originalClearTimeout(id);
    };

    window.setInterval = function(func, delay, ...params) {
        var id = originalSetTimeout(func, delay, ...params);
        __addTimer('interval', id, func, delay, ...params);
        return id;
    };

    window.clearInterval = function(id) {
        __clearTimer('interval', id);
        originalClearInterval(id);
    };
})();
